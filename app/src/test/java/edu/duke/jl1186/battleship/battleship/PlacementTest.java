package edu.duke.jl1186.battleship.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {
    @Test
    public void general_Test(){
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        assertEquals(p1, p2);

        Placement p3 = new Placement("A1H");
        Placement p4 = new Placement("A1h");
        assertEquals(p3, p4);
    }


}