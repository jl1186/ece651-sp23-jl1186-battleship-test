package edu.duke.jl1186.battleship.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
    }

    @Test
    public void test_empty_board() {
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(2, 3, 'X');
        Character[][] expect = new Character[2][3];
        expect[0][0] = 's';
        RectangleShip<Character> s = new RectangleShip<Character>(new Placement(new Coordinate(1, 1), 'H'), 's', '*');
        b.tryAddShip(s);
        checkWhatIsAtBoard(b, expect);
    }


    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected){
        int w = b.getWidth();
        int h = b.getHeight();
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                assertEquals(b.whatIsAt(new Coordinate(i, j), true), expected[i][j]);
            }
        }
    }

    @Test
    void getWidth() {
    }

    @Test
    void getHeight() {
    }

}