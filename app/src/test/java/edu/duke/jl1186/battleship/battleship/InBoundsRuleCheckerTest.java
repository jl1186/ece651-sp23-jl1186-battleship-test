package edu.duke.jl1186.battleship.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
    @Test
    public void test_checkMyRule() {
        BattleShipBoard<Character> board = new BattleShipBoard<Character>(10, 20, 'X');
        PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
        V1ShipFactory v1shipfactory = new V1ShipFactory();
        Coordinate c = new Coordinate("A0");
        Placement p = new Placement(c, 'h');
        Ship<Character> battShip = v1shipfactory.makeBattleship(p);

        // No error placeement
        assertNull(checker.checkMyRule(battShip, board, null));

        Coordinate c2 = new Coordinate("Z9");
        Placement p2 = new Placement(c2, 'v');
        Ship<Character> battShip2 = v1shipfactory.makeBattleship(p2);
        assertEquals("INVALID PLACEMENT: below the bottom!",
                checker.checkMyRule(battShip2, board,null));

        // Placement Out of bound
        Coordinate c3 = new Coordinate(-1, 0);
        Placement p3 = new Placement(c3, 'v');
        Ship<Character> battShip3 = v1shipfactory.makeBattleship(p3);
        assertEquals("INVALID PLACEMENT: above the top!",
                checker.checkPlacement(battShip3, board,null));

        // Placement Out of bound
        Coordinate c4 = new Coordinate(1, -1);
        Placement p4 = new Placement(c4, 'v');
        Ship<Character> battShip4 = v1shipfactory.makeBattleship(p4);
        assertEquals("INVALID PLACEMENT: beyond the left bounder!",
                checker.checkPlacement(battShip4, board,null));

        // Placement Out of bound
        Coordinate c5 = new Coordinate("F9");
        Placement p5 = new Placement(c5, 'h');
        Ship<Character> battShip5 = v1shipfactory.makeBattleship(p5);
        assertEquals("INVALID PLACEMENT: beyond the right bounder!",
                checker.checkPlacement(battShip5, board,null));
    }

}
