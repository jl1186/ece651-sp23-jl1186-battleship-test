package edu.duke.jl1186.battleship.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class RectangleShipTest {
    @Test
    void test_makeCoords(){
        RectangleShip<Character> s = new RectangleShip<Character>(new Placement(new Coordinate(1, 1), 'H'), 's', '*');
        HashSet<Coordinate> hs = new HashSet<>();
        hs = s.makeCoords(new Coordinate(1, 1), 1, 4);
        int j = 4;
        for(Coordinate i : hs){
            System.out.println(i.getRow() + " " + i.getColumn());
            assertEquals(i, new Coordinate(1, j));
            j--;
        }

    }

}