package edu.duke.jl1186.battleship.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter,
                           Coordinate... expectedLocs) {
        assertEquals(testShip.getName(), expectedName);

        for (Coordinate c : expectedLocs) {
            assertTrue(testShip.occupiesCoordinates(c));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
        }
    }

    @Test
    public void test_ship() {
        Placement p_v = new Placement(new Coordinate(1, 2), 'V');
        V1ShipFactory f = new V1ShipFactory();

        Ship<Character> sub_v = f.makeSubmarine(p_v);
        checkShip(sub_v, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

        Ship<Character> dst_v = f.makeDestroyer(p_v);
        checkShip(dst_v, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));

        Ship<Character> bat_v = f.makeBattleship(p_v);
        checkShip(bat_v, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2),
                new Coordinate(4, 2));

        Ship<Character> car_v = f.makeCarrier(p_v);
        checkShip(car_v, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2),
                new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));

        Placement p_h = new Placement(new Coordinate(1, 2), 'H');

        Ship<Character> sub_h = f.makeSubmarine(p_h);
        checkShip(sub_h, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

        Ship<Character> dst_h = f.makeDestroyer(p_h);
        checkShip(dst_h, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));

        Ship<Character> bat_h = f.makeBattleship(p_h);
        checkShip(bat_h, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
                new Coordinate(1, 5));

        Ship<Character> car_h = f.makeCarrier(p_h);
        checkShip(car_h, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
                new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));

    }


    public static void main(String[] args) {
        Placement p_v = new Placement(new Coordinate(1, 2), 'V');
        V1ShipFactory f = new V1ShipFactory();

        Ship<Character> sub_v = f.makeSubmarine(p_v);
        Board<Character> b = new BattleShipBoard<>(10, 20, 'X');
        b.tryAddShip((sub_v));
        BoardTextView btv = new BoardTextView(b);
        System.out.println(btv.displayMyOwnBoard());
    }
}