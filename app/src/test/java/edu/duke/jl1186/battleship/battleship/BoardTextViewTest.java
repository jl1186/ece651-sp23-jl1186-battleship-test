package edu.duke.jl1186.battleship.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    @Test
    public void test_display_empty_2by2() {
        Board<Character> b1 = new BattleShipBoard<Character>(2, 2, 'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader= "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_2by3(){
        Board<Character> b3x2 = new BattleShipBoard<Character>(2, 3, 'X');
        BoardTextView view = new BoardTextView(b3x2);
        String expectedHeader= "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        "C  |  C\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_5by3(){
        Board<Character> b3x2 = new BattleShipBoard<Character>(5, 3, 'X');
        RectangleShip<Character> rs1 = new RectangleShip<Character>(new Placement(new Coordinate(0, 4), 'H'), 's', '*');
        b3x2.tryAddShip(rs1);
        RectangleShip<Character> rs2 = new RectangleShip<Character>(new Placement(new Coordinate(1, 0), 'H'), 's', '*');
        b3x2.tryAddShip(rs2);
        RectangleShip<Character> rs3 = new RectangleShip<Character>(new Placement(new Coordinate(2, 3), 'H'), 's', '*');
        b3x2.tryAddShip(rs3);
        BoardTextView view = new BoardTextView(b3x2);
        String expectedHeader= "  0|1|2|3|4\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  | | | |s A\n"+
                        "B s| | | |  B\n"+
                        "C  | | |s|  C\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_displayAnyBoard(){
        BattleShipBoard<Character> board = new BattleShipBoard<Character>(4,3,'X');
        Coordinate c1 = new Coordinate("b0");
        Coordinate c2 = new Coordinate("a3");
        V1ShipFactory sf = new V1ShipFactory();
        Ship<Character> s1 = sf.makeSubmarine(new Placement(c1, 'H'));
        Ship<Character> s2 = sf.makeDestroyer(new Placement(c2, 'v'));
        board.tryAddShip(s1);
        board.tryAddShip(s2);
        BoardTextView view = new BoardTextView(board);

        String myView =
                        "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        //make sure we laid things out the way we think we did.
        assertEquals(myView, view.displayMyOwnBoard());

        String myView_hit =
                        "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B *|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";

        board.fireAt(c1);
        assertEquals(myView_hit, view.displayMyOwnBoard());

        String enemyView_hit =
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        assertEquals(enemyView_hit, view.displayEnemyBoard());

        String enemyView_hit_Enemy =
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C X| | |  C\n" +
                        "  0|1|2|3\n";
        Coordinate c3 = new Coordinate("c0");
        board.fireAt(c3);
        assertEquals(enemyView_hit_Enemy, view.displayEnemyBoard());
    }


}