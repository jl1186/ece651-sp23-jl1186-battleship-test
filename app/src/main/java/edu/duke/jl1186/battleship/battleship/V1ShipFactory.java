package edu.duke.jl1186.battleship.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }


    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        char orientation = where.getOrientation();
        RectangleShip<Character> ship;
        if (orientation == 'V') {
            ship = new RectangleShip<Character>(name, where, w, h, letter, '*');
        } else {
            ship = new RectangleShip<Character>(name, where, h, w, letter, '*');
        }
        return ship;
    }

}