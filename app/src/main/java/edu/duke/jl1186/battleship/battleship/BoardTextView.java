package edu.duke.jl1186.battleship.battleship;

import java.util.ArrayList;
import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the enemy's board.
 */
public class BoardTextView {
    /** The Board to display */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board it will display.
     *
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

//    public String displayMyOwnBoard() {
//        StringBuilder ans = new StringBuilder("");
//        ans.append(makeHeader());
//        for(int i = 0; i < toDisplay.getHeight(); i++){
//            ans.append((char) ('A' + i));
//            ans.append(" ");
//            for(int j = 0; j < toDisplay.getWidth(); j++){
//                if(toDisplay.whatIsAt(new Coordinate(i, j), true) != null){
//                    char c = toDisplay.whatIsAt(new Coordinate(i, j), true);
//                    ans.append(c);
//                    if(j != toDisplay.getWidth() - 1) ans.append("|");
//                }else{
//                    if(j != toDisplay.getWidth() - 1) ans.append(" |");
//                    else ans.append(" ");
//                }
//            }
//            ans.append(" ");
//            ans.append((char) ('A' + i));
//            ans.append('\n');
//        }
//        ans.append(makeHeader());
//        return ans.toString();
//    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");

        return ans.toString();
    }

    private String makeHeader(String myHeader, String enemyHeader) {
        String spaceOfFirstHeader = "     ";
        int w = this.toDisplay.getWidth();
        int secondHeaderStart = 2 * w + 22;
        int myHeaderLength = myHeader.length();
        int spaceBetweenHeader = secondHeaderStart - 5 - myHeaderLength;
        String spaceOfGapBetweenHeader = makeSpace(spaceBetweenHeader);
        return spaceOfFirstHeader + myHeader + spaceOfGapBetweenHeader +  enemyHeader + "\n";
    }

    public String makeSpace(int size){
        return "" + " ".repeat(Math.max(0, size));
    }

    public String makeContent(Function<Coordinate, Character> getSquareFn){
        StringBuilder ans = new StringBuilder("");
        for(int i = 0; i < toDisplay.getHeight(); i++){
            ans.append((char) ('A' + i));
            ans.append(" ");
            for(int j = 0; j < toDisplay.getWidth(); j++){
                Coordinate c = new Coordinate(i,j);
                if (j != toDisplay.getWidth() - 1) {
                    ans.append(getSquareFn.apply(c) != null ?getSquareFn.apply(c):" ");
                    ans.append("|");
                }
                else{
                    ans.append(getSquareFn.apply(c) != null ? getSquareFn.apply(c):" ");
                    ans.append(" " + (char)('A' + i) + "\n");
                }
            }
        }
        return ans.toString();
    }

    public String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder("");
        ans.append(makeHeader());
        ans.append(makeContent(getSquareFn));
        ans.append(makeHeader());
        return ans.toString();
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForSelf);
    }

    public String displayEnemyBoard(){
        return displayAnyBoard(toDisplay::whatIsAtForEnemy);
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        StringBuilder ans = new StringBuilder();
        String spaceOfBoard = makeSpace(16);
        String offset = "  ";
        String header =  makeHeader(myHeader, enemyHeader);
        ans.append(header);

        // Spilt the board
        String[] myViewSpilt = displayMyOwnBoard().split("\n");
        String[] enemyViewSpilt = enemyView.displayEnemyBoard().split("\n");
        ArrayList<String> mergeView = new ArrayList<>();
        for (int i = 0; i < myViewSpilt.length; i++) {
            // If not the first line or the last line
            if(i != 0 && i != myViewSpilt.length - 1){
                mergeView.add(myViewSpilt[i] + spaceOfBoard + enemyViewSpilt[i]);
            }
            else {
                mergeView.add(myViewSpilt[i] + spaceOfBoard + offset + enemyViewSpilt[i]);
            }
            ans.append(mergeView.get(i)).append("\n");
        }
        return ans.toString();
    }
}
