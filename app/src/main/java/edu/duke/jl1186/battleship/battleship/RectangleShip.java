package edu.duke.jl1186.battleship.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T>{
    final String name;
    final private int width;
    final private int height;
    private char orientation;

    public RectangleShip(String name, Placement upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft.getCoordinate(), width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
        this.orientation = upperLeft.orientation;
        this.width = width;
        this.height = height;
    }

    public RectangleShip(String name, Placement upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    public RectangleShip(Placement upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height){
        HashSet<Coordinate> hs = new HashSet<>();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                hs.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
            }
        }
        return hs;
    }

    public String getName(){
        return name;
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public char getOrientation() {
        return this.orientation;
    }

    public void setOrientation(char orientation) {
        this.orientation = orientation;
    }
}
