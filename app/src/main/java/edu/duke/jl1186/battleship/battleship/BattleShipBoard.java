package edu.duke.jl1186.battleship.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T>{
    private final int width;
    private final int height;

    final ArrayList<Ship<T>> myShips;

    private final PlacementRuleChecker<T> placementChecker;

    private final T missInfo;

    private HashSet<Coordinate> enemyMisses;
    private HashMap<Coordinate, T> enemyViewOfHitShip;
    private HashMap<Coordinate, T> enemyViewOfHitShipAfterMove;

    public int getWidth(){
        return this.width;
    }

    public int getHeight(){
        return this.height;
    }

    /**
     * Constructs a BattleShipBoard with the specified width
     * and height
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int w, int h, T missInfo) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.myShips = new ArrayList<Ship<T>>();

        this.placementChecker = new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null));
        this.missInfo = missInfo;

        this.enemyMisses = new HashSet<Coordinate>();
        this.enemyViewOfHitShip = new HashMap<Coordinate, T>();
        this.enemyViewOfHitShipAfterMove = new HashMap<Coordinate, T>();
    }

    /**
     * Add a ship to the current board
     * @param toAdd is the ship to be added
     */
    public String tryAddShip(Ship<T> toAdd){
        String check = this.placementChecker.checkPlacement(toAdd, this, null);
        if(check != null) return check;
        myShips.add(toAdd);
        return null;
    }

    /**
     * Check what's the object on the specific coordinate
     * @param where is the coordinate to check
     * @param isMyself is whether it is my own board or enemy's board
     * @return
     */
    public T whatIsAt(Coordinate where, boolean isMyself) {
        if(isMyself){
            for (Ship<T> s: myShips) {
                if (s.occupiesCoordinates(where)){
                    return s.getDisplayInfoAt(where, true);
                }
            }
        }else{
            for (Ship<T> s : myShips) {
                // Case 1: show where enemy miss
                if(enemyMisses.contains(where)) return missInfo;
                // Case 2: show enemy's hits on our ships
                if(enemyViewOfHitShip.containsKey(where)) return enemyViewOfHitShip.get(where);
                // Case 3: show the point after we move the ship
                if(enemyViewOfHitShipAfterMove.containsKey(where)) return null;
                // Case 4: show un-hit points
                if(s.occupiesCoordinates(where)) return s.getDisplayInfoAt(where, false);
            }
        }
        return null;
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * Fire at the certain coordinate
     * @param c is the coordinate to fire
     * @return
     */
    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> ship : myShips) {
            if (ship.occupiesCoordinates(c)) {
                ship.recordHitAt(c);
                return ship;
            }
        }
        enemyMisses.add(c);
        return null;
    }


    /**
     * Find out the coordinate belongs to which ship
     * @param c is the coordinate to find out
     * @return
     */
    @Override
    public Ship<T> whichShip(Coordinate c) {
        if (whatIsAt(c, true) == null) return null;
        for(Ship<T> ship: myShips){
            for(Coordinate shipCoordinate: ship.getCoordinates()){
                if(shipCoordinate.equals(c)){
                    return ship;
                }
            }
        }
        return null;
    }

    /**
     * Record the enemyView of this ship hit coordinates and ship type
     * @param shipToBeMv is the ship that will be moved away
     * @param enemyViewOfHitShip is the view from the enemy to the ship
     * @param before is whether it is hit before
     */
    private void recordMoveShipHitInfo(Ship<T> shipToBeMv, HashMap<Coordinate, T> enemyViewOfHitShip, boolean before){
        Iterable<Coordinate> shipCoors = shipToBeMv.getCoordinates();
        for(Coordinate c: shipCoors){
            if(whatIsAtForEnemy(c) != null){
                enemyViewOfHitShip.put(c,whatIsAtForEnemy(c));
            }
            if(!before){
                enemyViewOfHitShip.put(c,whatIsAtForEnemy(c));
            }
        }
    }

    /**
     * Try to move ship to another position
     * @param from is the original position
     * @param to is the destination
     * @param DestOrientation is the target orientation
     * @return
     */
    @Override
    public String tryMoveShip(Coordinate from, Coordinate to, char DestOrientation) {
        Ship<T> shipToBeMv = whichShip(from);
        if(shipToBeMv == null) return "INVALID POSITION: no ship there, try again!";


        recordMoveShipHitInfo(shipToBeMv, this.enemyViewOfHitShip, true);

        Iterable<Coordinate> originCoordinate = shipToBeMv.getCoordinates();
        Ship<T> baitShip = null;
        String shipName = shipToBeMv.getName();
        if(shipName.equals("Submarine") || shipName.equals("Destroyer")){
            baitShip = getBat_Rect(to, DestOrientation, shipToBeMv, shipName);
        }
        else{
            baitShip = getBat_Poly(to, DestOrientation, shipToBeMv, shipName);
        }

        // Validate the location
        String result = this.placementChecker.checkPlacement(baitShip, this, originCoordinate);
        if(result != null) return result;

        // Move the real ship to the position
        moveShip(to, DestOrientation, shipToBeMv);

        // Record the before move coordinates and view
        recordMoveShipHitInfo(shipToBeMv,this.enemyViewOfHitShipAfterMove, false);
        return null;
    }

    /**
     * Get a bait polygon ship
     * @param to
     * @param DestOrientation
     * @param shipToBeMv
     * @param shipName
     * @return
     */
    private Ship<T> getBat_Poly(Coordinate to, char DestOrientation, Ship<T> shipToBeMv, String shipName) {
        Ship<T> baitShip;
        PolygonShip<Character> shipToBeMv_ = (PolygonShip<Character>) shipToBeMv;
        Placement p = new Placement(shipToBeMv.getLeftTopCoordinate(), shipToBeMv.getOrientation());
        baitShip = (Ship<T>) new PolygonShip<Character>(shipName, shipName ,p, shipToBeMv_.getWidth(), shipToBeMv_.getHeight(), 't', '*');

        moveShip(to, DestOrientation, baitShip);
        return baitShip;
    }

    /**
     * Get the bait ships of the rectangle ships
     * @param to is the destination
     * @param DestOrientation is the destinated orientation
     * @param shipToBeMv is the ship to be moved
     * @param shipName is the name of the ship
     * @return
     */
    private Ship<T> getBat_Rect(Coordinate to, char DestOrientation, Ship<T> shipToBeMv, String shipName) {
        Ship<T> baitShip;
        RectangleShip<Character> shipToBeMv_ = (RectangleShip<Character>) shipToBeMv;
        Placement p = new Placement(shipToBeMv.getLeftTopCoordinate(), shipToBeMv.getOrientation());
        baitShip = (Ship<T>) new RectangleShip<Character>(shipName, p, shipToBeMv_.getWidth(), shipToBeMv_.getHeight(), 't', '*');

        // Check curr orientation
        char currOrientation = baitShip.getOrientation();

        // Check the ship type
        String shipType = baitShip.getName();

        int rotateTimes = getRotateTimes(DestOrientation, currOrientation, shipType);

        // Rotate the ship
        for (int i = 0; i < rotateTimes; i++) {
            baitShip.rotateShip();
        }

        // Correct the location
        Coordinate wrongCoordinate =  baitShip.getLeftTopCoordinate();
        int row_offset = to.getRow() - wrongCoordinate.getRow();
        int col_offset = to.getColumn() - wrongCoordinate.getColumn();

        baitShip.shiftShip(row_offset, col_offset);
        return baitShip;
    }

    /**
     * Get the index of orientation for rectangle ships
     * @param currOrientation is the current orientation
     * @return
     */
    private int getIdxOfOrientation_Sub_Dest(char currOrientation) {
        int currOrientationIdx = -1;
        if(currOrientation == 'H') currOrientationIdx = 0;
        else if (currOrientation == 'V') currOrientationIdx = 1;
        return currOrientationIdx;
    }

    /**
     * Get the index of orientation for polygon ships
     * @param currOrientation is the current orientation
     * @return
     */
    private int getIdxOfOrientation_Bat_Car(char currOrientation) {
        int currOrientationIdx = -1;
        switch (currOrientation){
            case 'U' -> currOrientationIdx = 0;
            case 'R' -> currOrientationIdx = 1;
            case 'D' -> currOrientationIdx = 2;
            case 'L' -> currOrientationIdx = 3;
        }
        return currOrientationIdx;
    }

    /**
     * Get the rotation time for the position change
     * @param DestOrientation is the destination orientation
     * @param currOrientation is the current orientation
     * @param shipType is the type of the ship
     * @return
     */
    private int getRotateTimes(char DestOrientation, char currOrientation, String shipType) {
        if(shipType.equals("Submarine") || shipType.equals("Destroyer")){
            int currOrientationIdx = getIdxOfOrientation_Sub_Dest(currOrientation);
            int destOrientationIdx = getIdxOfOrientation_Sub_Dest(DestOrientation);
            if(currOrientationIdx > destOrientationIdx){
                destOrientationIdx += 2;
            }
            return destOrientationIdx - currOrientationIdx;
        }
        else{
            int currOrientationIdx = getIdxOfOrientation_Bat_Car(currOrientation);
            int destOrientationIdx = getIdxOfOrientation_Bat_Car(DestOrientation);
            if(currOrientationIdx > destOrientationIdx){
                destOrientationIdx += 4;
            }
            return destOrientationIdx - currOrientationIdx;
        }
    }

    /**
     * Move the ship
     * @param to is the destination
     * @param DestOrientation is the destinated orientation
     * @param baitShip is the bait ship
     */
    private void moveShip(Coordinate to, char DestOrientation, Ship<T> baitShip) {
        // Check curr orientation
        char currOrientation = baitShip.getOrientation();

        // Check the ship type
        String shipType = baitShip.getName();

        int rotateTimes = getRotateTimes(DestOrientation, currOrientation, shipType);

        // Rotate the ship
        for (int i = 0; i < rotateTimes; i++) {
            baitShip.rotateShip();
        }

        // Correct the location
        Coordinate wrongCoordinate =  baitShip.getLeftTopCoordinate();
        int row_offset = to.getRow() - wrongCoordinate.getRow();
        int col_offset = to.getColumn() - wrongCoordinate.getColumn();

        baitShip.shiftShip(row_offset, col_offset);
        // Update the ship orientation
        baitShip.setOrientation(DestOrientation);
    }

    /**
     * Check whether the game is lost
     * @return
     */
    @Override
    public boolean checkLose() {
        for(Ship<T> ship: myShips){
            if (!ship.isSunk()) return false;
        }
        return true;
    }

}
