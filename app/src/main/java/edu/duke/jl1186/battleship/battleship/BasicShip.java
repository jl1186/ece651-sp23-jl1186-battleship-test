package edu.duke.jl1186.battleship.battleship;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T>{
    //    final Coordinate myLocation;
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    /**
     * Constructor for BasicShip
     * @param where the position for the ship
     * @param myDisplayInfo the info to display
     * @param enemyDisplayInfo the enemy's information
     */
    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo){
        myPieces = new HashMap<Coordinate, Boolean>();
        for(Coordinate c : where) myPieces.put(c, false);
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }

//    public BasicShip(Iterable<Coordinate> where){
//        myPieces = new HashMap<Coordinate, Boolean>();
//        for(Coordinate c : where){
//            myPieces.put(c, false);
//        }
//    }

    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    @Override
    public boolean isSunk() {
        boolean sunk = true;
        for(Coordinate c : myPieces.keySet()){
            if(!myPieces.get(c)){
                sunk = false;
                break;
            }
        }
        return sunk;
    }

    @Override
    public void recordHitAt(Coordinate where) {
        myPieces.put(where, true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) {
        return myPieces.get(where);
    }

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        return myShip ? myDisplayInfo.getInfo(where, this.wasHitAt(where)) : enemyDisplayInfo.getInfo(where, this.wasHitAt(where));
    }

    /**
     * Check if coordinate c is part of the ship
     * @param c is the coordinate to check
     */
    protected void checkCoordinateInThisShip(Coordinate c){
        if(!this.occupiesCoordinates(c)) throw new IllegalArgumentException();
    }

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return this.myPieces.keySet();
    }

    /**
     * Get the anchor point of the current ship
     * @return
     */
    public Coordinate getLeftTopCoordinate(){
        int minX = 10, minY = 26;
        for (Coordinate key : myPieces.keySet()) {
            minX = Math.min(key.getRow(), minX);
            minY = Math.min(key.getColumn(), minY);
        }
        return new Coordinate(minX, minY);
    }

    /**
     * Rotate the current ship
     */
    public void rotateShip(){
        HashMap<Coordinate, Boolean> pieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c : myPieces.keySet()) {
            Coordinate newCoordination = new Coordinate(c.getColumn(), -1 * c.getRow() );
            pieces.put(newCoordination, myPieces.get(c));
        }
        this.myPieces = pieces;
        Coordinate c = getLeftTopCoordinate();
        int offset = Math.min(c.getRow(),c.getColumn());
        shiftShip(-1 * offset, -1 * offset);
    }

    /**
     * Shift the ship
     * @param rows
     * @param cols
     */
    public void shiftShip(int rows, int cols){
        HashMap<Coordinate, Boolean> pieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate key : myPieces.keySet()) {
            Coordinate newCoordination = new Coordinate(key.getRow() + rows, key.getColumn() + cols);
            pieces.put(newCoordination, myPieces.get(key));
        }
        this.myPieces = pieces;
    }

}
