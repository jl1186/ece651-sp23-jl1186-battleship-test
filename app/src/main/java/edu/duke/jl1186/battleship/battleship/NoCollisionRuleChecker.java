package edu.duke.jl1186.battleship.battleship;

import java.util.HashSet;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> b, Iterable<Coordinate> coordinates) {
        if (coordinates == null) {
            for (Coordinate c : theShip.getCoordinates()) {
                if (b.whatIsAtForSelf(c) != null) return "That placement is invalid: the ship overlaps another ship.";
            }
            return null;
        } else {
            HashSet<Coordinate> hs = new HashSet<Coordinate>();
            for (Coordinate c : coordinates) {
                hs.add(c);
            }

            for (Coordinate c : theShip.getCoordinates()) {
                if (b.whatIsAtForSelf(c) != null) {
                    if (hs.contains(c)) continue;
                    return "That placement is invalid: the ship overlaps another ship.";
                }
            }
            return null;

        }
    }
}
