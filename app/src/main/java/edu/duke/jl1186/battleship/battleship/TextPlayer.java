package edu.duke.jl1186.battleship.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;

public class TextPlayer {
    final String name;

    final Board<Character> theBoard;
    final BoardTextView view;

    final BufferedReader inputReader;
    final PrintStream out;

    final AbstractShipFactory<Character> shipFactory;

    final ArrayList<String> shipsToPlace;

    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;

    private int moveRemaining;
    private int scanRemaining;

    private final boolean isComputer;

    private final Random random;

    public TextPlayer(String name, Board<Character> theBoard, BufferedReader br, PrintStream out, AbstractShipFactory<Character> factory, boolean isComputer) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = br;
        this.out = out;
        this.shipFactory = factory;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        this.moveRemaining = 2;
        this.scanRemaining = 1;
        this.isComputer = isComputer;
        this.random = new Random();
        setupShipCreationList();
        setupShipCreationMap();
    }

    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", shipFactory::makeSubmarine);
        shipCreationFns.put("Carrier", shipFactory::makeCarrier);
        shipCreationFns.put("Destroyer", shipFactory::makeDestroyer);
        shipCreationFns.put("BattleShip", shipFactory::makeBattleship);
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(1, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(1, "Carrier"));
        shipsToPlace.addAll(Collections.nCopies(1, "BattleShip"));
        shipsToPlace.addAll(Collections.nCopies(1, "Destroyer"));
    }

    /**
     * Read the user's choice on the special function
     * @return
     */
    private String readUserChoice() {
        String input = null;
        boolean reEnterFlag = true;
        while (reEnterFlag){
            try {
                input = inputReader.readLine().toUpperCase();
                // the input is valid
                if( input.equals("F") || input.equals("M") || input.equals("S")) reEnterFlag = false;
                // the input is invalid
                else{
                    out.println("INVALID COORDINATE, please re-enter!");
                }
            }catch (IOException e) {
                out.println("INVALID COORDINATE, please re-enter!");
            }
        }
        return input;
    }

    /**
     * One round of the game
     * @param enemyBoard the board of the enemy
     * @param view the view of our board
     * @param playerName the name of the current player
     */
    public void oneRound(Board<Character> enemyBoard, BoardTextView view, String playerName){
        String prompt = playerName + "'s turn:\n " +
                "F Fire at a square\n" +
                " M Move a ship to another square (" + moveRemaining+  " remaining)\n" +
                " S Sonar scan (" + scanRemaining + " remaining)\n" +
                "\n" +
                "Player " + playerName + ", what would you like to do?";
        
        String choice = null;
        
        // if it is computer, then it just fire
        if(isComputer) choice = "F";
        // if it is human player, it reads the choice from users
        else {
            out.println(prompt);
            choice= readUserChoice();
        }
        
        if(choice.equals("F")){
            attack(enemyBoard, view, playerName);
        }
        else if(choice.equals("M") && moveRemaining > 0){
            move(enemyBoard, view, playerName);
            moveRemaining--;
        }
        else if(choice.equals("S") && scanRemaining > 0){
            sonar(enemyBoard,view,playerName);
            scanRemaining--;
        }
        else {
            out.println("There is no chance to use '" + choice + "' again, plz re-enter.");
            oneRound(enemyBoard, view, playerName);
        }
    }

    /**
     * Get the placement information
     * @param prompt is the hint
     * @param getCoordinateOnly is whether we only take coordinate
     * @return
     * @throws IOException
     */
    public Placement readPlacement(String prompt, boolean getCoordinateOnly) throws IOException {
        this.out.println(prompt);
        String s = null;
        // generate a random input for computer
        if(this.isComputer){
            String row = String.valueOf((char)(random.nextInt(20) + 'a'));
            String col = String.valueOf(random.nextInt(10));

            int v_or_h = random.nextInt(10);
            if(v_or_h % 2 == 0) s = row + col + "H";
            else s = row + col + "V";
        }
        else{
            s = inputReader.readLine();
        }

        if (s == null) throw new IOException("Placement can't be null!");

//        if(!this.isComputer && getCoordinateOnly) s += "H";
        return new Placement(s);
    }

    /**
     * re-read the placement if we meet any invalid input
     * @param prompt is the hint
     * @param getCoordinateOnly
     * @return
     */
    public Placement reReadPlacement(String prompt, boolean getCoordinateOnly){
        Placement placementRes = null;
        boolean reEnter= true;
        while (reEnter){
            try {
                placementRes = readPlacement(prompt, getCoordinateOnly);
                reEnter = false;
            } catch (IOException e) {
                if(!isComputer) this.out.println("Invalid Coordinate(Empty input), please enter again!");
            } catch (IllegalArgumentException e){
                if(!isComputer) this.out.println("Invalid Coordinate, please enter again!");
            }
        }

        return placementRes;
    }

    /**
     * Attack on a coordinate
     * @param enemyBoard the board of the enemy
     * @param view the view of our board
     * @param playerName the name of the player
     */
    public void attack(Board<Character> enemyBoard, BoardTextView view, String playerName){
        String turnPrompt = "Player " + playerName + "'s turn";
        String attackPrompt = "Player " + playerName + " select a coordinate to attack!";
        if(!isComputer) this.out.println(turnPrompt);

        // Display the enemy board
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        if(!this.isComputer) this.out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,"Your board", "Opponent's board"));

        Coordinate attackCoordinate = null;
        if(!this.isComputer) attackCoordinate = reReadPlacement(attackPrompt, true).getCoordinate();
        else{
            attackCoordinate = reReadPlacement("", true).getCoordinate();
        }

        Ship<Character> ship = enemyBoard.fireAt(attackCoordinate);

        // Give the attack feedback
        if(!this.isComputer){
            if(ship == null)  this.out.println("You missed!");
            else this.out.println("You hit a " + ship.getName() + "!");
        }
        else {
            if(ship == null)  this.out.println("Player " + playerName + " miss! (Computer)\n");
            else this.out.println("Player " + playerName + " hit your " + ship.getName() + " at " + attackCoordinate + " !\n");
        }
    }

    /**
     * Move the ship
     * @param enemyBoard
     * @param view
     * @param playerName
     */
    public void move(Board<Character> enemyBoard, BoardTextView view, String playerName){
        // Display the enemy board
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        this.out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,"Your board", "Opponent's board"));

        String prompt_from = "Player " + playerName + " select a coordinate of ship to move!";

        Coordinate from = reReadPlacement(prompt_from, true).getCoordinate();

        String prompt_to = "Player " + playerName + " select a placement move to!";
        Placement to = reReadPlacement(prompt_to, false);


        String result = this.theBoard.tryMoveShip(from,to.getCoordinate(),to.getOrientation());
        if(result != null){
            out.println(result);
            move(enemyBoard, view, playerName);
        }
        this.out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,"Your board", "Opponent's board"));
    }

    /**
     * Sonar the area
     * @param enemyBoard
     * @param view
     * @param playerName
     */
    public void sonar(Board<Character> enemyBoard, BoardTextView view, String playerName){
        String turnPrompt = "Player " + playerName + "'s turn";
        String attackPrompt = "Player " + playerName + " select a coordinate to scan!";
        this.out.println(turnPrompt);

        // Display the enemy board
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        this.out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,"Your board", "Opponent's board"));

        Coordinate sonarCoordinate = reReadPlacement(attackPrompt, true).getCoordinate();
        int row_offset = sonarCoordinate.getRow();
        int col_offset = sonarCoordinate.getColumn();
        ArrayList<Coordinate> sonarList = new ArrayList<Coordinate>(
                Arrays.asList(
                        new Coordinate(-3 + row_offset, col_offset),
                        new Coordinate(-2 + row_offset,-1 + col_offset),
                        new Coordinate(-2 + row_offset, col_offset),
                        new Coordinate(-2 + row_offset,1 + col_offset),
                        new Coordinate(-1 + row_offset,-2 + col_offset),
                        new Coordinate(-1 + row_offset,-1 + col_offset),
                        new Coordinate(-1 + row_offset, col_offset),
                        new Coordinate(-1 + row_offset,1 + col_offset),
                        new Coordinate(-1 + row_offset,2 + col_offset),
                        new Coordinate(row_offset,-3 + col_offset),
                        new Coordinate(row_offset,-2 + col_offset),
                        new Coordinate(row_offset,-1 + col_offset),
                        new Coordinate(row_offset, col_offset),
                        new Coordinate(row_offset,1 + col_offset),
                        new Coordinate(row_offset,2 + col_offset),
                        new Coordinate(row_offset,3 + col_offset),
                        new Coordinate(1 + row_offset,-2 + col_offset),
                        new Coordinate(1 + row_offset,-1 + col_offset),
                        new Coordinate(1 + row_offset, col_offset),
                        new Coordinate(1 + row_offset,1 + col_offset),
                        new Coordinate(1 + row_offset,2 + col_offset),
                        new Coordinate(2 + row_offset,-1 + col_offset),
                        new Coordinate(2 + row_offset, col_offset),
                        new Coordinate(2 + row_offset,1 + col_offset),
                        new Coordinate(3 + row_offset, col_offset)
                )
        );

        int SubmarinesNum = 0, DestroyersNum = 0, BattleshipsNum = 0, CarriersNum = 0;
        for(Coordinate c: sonarList){
            Ship<Character> s = enemyBoard.whichShip(c);
            if(s != null){
                switch (s.getName()) {
                    case "Submarine" -> SubmarinesNum++;
                    case "Destroyer" -> DestroyersNum++;
                    case "Battleship" -> BattleshipsNum++;
                    case "Carrier" -> CarriersNum++;
                }
            }
        }

        String ans = "Submarines occupy " + SubmarinesNum + " squares\n"+
                "Destroyers occupy " + DestroyersNum + " squares\n"+
                "Battleships occupy " + BattleshipsNum + " squares\n"+
                "Carriers occupy " + CarriersNum + " squares\n";
        out.println(ans);
    }

    public void doPlacementPhase() throws IOException {
        String instruction = "Player " + this.name + ": you are going to place the following ships (which are all\n"
                + "rectangular). For each ship, type the coordinate of the upper left\n"
                + "side of the ship, followed by either H (for horizontal) or V (for\n"
                + "vertical).  For example M4H would place a ship horizontally starting\n"
                + "at M4 and going to the right.  You have\n\n" +

                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n";

        if(!this.isComputer) this.out.print(view.displayMyOwnBoard() + instruction);
        for (String s : shipsToPlace) doOnePlacement(s, shipCreationFns.get(s));
    }

    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        boolean reEnter = true;
        // the human player
        if(!this.isComputer){
            while (reEnter){
                Placement p = reReadPlacement("Player " + name + " where do you want to place a " + shipName + "?",false);
                Ship<Character> s = createFn.apply(p);
                String result = theBoard.tryAddShip(s);
                if(result != null) out.println(result);
                else reEnter = false;
            }
            out.print(view.displayMyOwnBoard());
        }
        // the computer will automatically generate it
        else {
            while (reEnter){
                Placement p = reReadPlacement("",false);
                Ship<Character> s = createFn.apply(p);
                String result = theBoard.tryAddShip(s);
                if(result != null) reEnter = false;
            }
        }
    }

}
