package edu.duke.jl1186.battleship.battleship;

public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;
    //more stuff

    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard, Iterable<Coordinate> coordinates);

    public String checkPlacement (Ship<T> theShip, Board<T> theBoard, Iterable<Coordinate> coordinates) {
        //if we fail our own rule: stop the placement is not legal
        if (checkMyRule(theShip, theBoard, coordinates) != null) {
            return checkMyRule(theShip, theBoard, coordinates);
        }
        //other wise, ask the rest of the chain.
        if (next != null) {
            return next.checkPlacement(theShip, theBoard, coordinates);
        }
        //if there are no more rules, then the placement is legal
        return null;
    }
}
