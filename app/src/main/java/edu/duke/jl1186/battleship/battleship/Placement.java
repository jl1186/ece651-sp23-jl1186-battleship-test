package edu.duke.jl1186.battleship.battleship;

import org.checkerframework.checker.units.qual.C;

import java.util.Locale;
import java.util.Objects;

public class Placement {
    final Coordinate where;
    final char orientation;

    public Placement(Coordinate where, char orientation) {
        this.where = where;
        String ori = String.valueOf(orientation).toUpperCase();
        this.orientation = ori.charAt(0);
    }

    public Placement(String str){
        if(str.length() != 3 && str.length() != 2) throw new IllegalArgumentException("Placement length should be 3!");
        where = new Coordinate(str.substring(0, 2));
        if(str.length() == 3){
            String ori = str.substring(2,3).toUpperCase();
            if(!(ori.equals("V") || ori.equals("H") || ori.equals("L") || ori.equals("R")
                    || ori.equals("U") || ori.equals("D"))){
                throw new IllegalArgumentException("The orientation is not correct!");
            }
            orientation = ori.charAt(0);
        }else{
            orientation = 'H';
        }

    }

    public Coordinate getCoordinate() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }

    @Override
    public String toString() {
        return "Placement{" +
                "where=" + where +
                ", orientation=" + orientation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Placement placement = (Placement) o;
        return orientation == placement.orientation && Objects.equals(where, placement.where);
    }

    @Override
    public int hashCode() {
        return Objects.hash(where, orientation);
    }


}
