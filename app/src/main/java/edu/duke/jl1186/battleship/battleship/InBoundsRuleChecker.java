package edu.duke.jl1186.battleship.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T>{

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> b, Iterable<Coordinate> coordinates) {
        // TODO Auto-generated method stub
        if(theShip == null)  return "That placement is invalid: it does not have the correct format.";
        for (Coordinate c : theShip.getCoordinates()) {
            if (0 > c.getRow()) return "That placement is invalid: the ship goes off the top of the board.";
            else if (c.getRow() >= b.getHeight()) return "That placement is invalid: the ship goes off the bottom of the board.";
            else if (0 > c.getColumn()) return "That placement is invalid: the ship goes off the left of the board.";
            else if (c.getColumn() >= b.getWidth()) return "That placement is invalid: the ship goes off the right of the board.";
        }
        return null;
    }
}
