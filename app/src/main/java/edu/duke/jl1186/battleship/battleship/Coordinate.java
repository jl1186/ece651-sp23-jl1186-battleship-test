package edu.duke.jl1186.battleship.battleship;

import java.util.Objects;

public class Coordinate {
    private final int row;
    private final int column;

    public int getRow(){
        return this.row;
    }

    public int getColumn(){
        return this.column;
    }

    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public Coordinate(String descr){
        if(descr.length() != 2) throw new IllegalArgumentException();

        char r = descr.charAt(0);
        if(r >= 'A' && r <= 'Z') row = r - 'A';
        else if(r >= 'a' && r <= 'z') row = r - 'a';
        else throw new IllegalArgumentException();

        char c = descr.charAt(1);
        if(c >= '0' && c <= '9') column = c - '0';
        else throw new IllegalArgumentException();
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
