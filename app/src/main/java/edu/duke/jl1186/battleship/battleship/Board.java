package edu.duke.jl1186.battleship.battleship;

public interface Board<T> {
    public int getWidth();
    public int getHeight();

    public String tryAddShip(Ship<T> toAdd);

    public T whatIsAtForSelf(Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);

    public T whatIsAt(Coordinate where, boolean isMyself);

    public Ship<T> fireAt(Coordinate c);
    public Ship<T> whichShip(Coordinate c);
    public String tryMoveShip(Coordinate from, Coordinate to, char orientation);

    public boolean checkLose();
}
